# Dictionaries / objects

band = {
    "vocals" : "Morrissey",
    "guitar" : "Marr"
}

band2 = dict(vocals="Morrissey", guitar="Marr")

print(band)
print(band2)

# Access items

print(band["guitar"])
print(band.get("guitar"))

# List all keys 
print(band.keys())

print(band.values())

# List all key/value pairs as tuples
print(band.items())

# Verify if a key exists
print("guitar" in band)
print("triangle" in band)

# change values
band ["guitar"] = "Clapton"
band.update({"bass" : "JPJ"})

print(band)

# remove items
print(band.pop("bass"))
print(band)

# nested dictionaries

member = {
    "name" : "Mark"
}

member2 = {
    "name" : "Jack"
}

band = {
    "member1" : member["name"],
    "member2" : member2["name"]
}

print(band)

# SETS

print("")

nums = { 1 , 2, 3, 4}

nums2 = set((1,2,3,4))

print(nums)
print(nums2)
print(type(nums))

# no duplicate - in the set - allowed
nums = { 1 ,2 ,2, 2, 3}
print(nums)

# True is a duplicate of 1 and false is a duplicate of 0 
nums = {1 , True, 2, False, 3, 4, 0}
print(nums)


