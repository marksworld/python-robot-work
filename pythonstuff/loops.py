# while loop basic
value = 1 

# while value < 10:
#     print(value)
#     if value == 5:
#         break
#     value += 1

# while value < 10:
#     value += 1
#     if value == 5:
#         continue
#     print(value)
# else:
#     print("Value is now equal to " + str(value))

# for loop example
names = ["Dave", "Sarah", "James"]
for name in names:
    print(name)

for x in "Mississippi":
    print(x)

for x in names:
    if x == "Sarah":
        continue #right so this will break for this loop run only and the continue the loop
    print(x)

for x in range(4):
    print(x)
    