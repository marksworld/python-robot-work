# user input rock paper scissors
import sys
import random
from enum import Enum

# recursion and while loop

def rps():
    game_count = 0
    player_wins = 0
    python_wins = 0

    def play_rps():
        nonlocal player_wins
        nonlocal python_wins

        class RPS(Enum):
            ROCK = 1
            PAPER = 2
            SCISSORS = 3

        #print(RPS(2))
        #print(RPS.ROCK)
        #print(RPS['ROCK'])
        #print(RPS.ROCK.value)

        print("")

        playerchoice = input("Enter...\n1 for Rock,\n2 for paper,\n3 for scissors\n\n")

        if playerchoice not in ["1","2","3"]:
            print("You need to enter 1, 2 or 3.")
            return play_rps()

        player = int(playerchoice)

        computerchoice = int(random.choice("123"))

        print("")
        print("You chose " + str(RPS(player)).replace('RPS.', '') + ".")
        print("Python chose " + str(RPS(computerchoice)).replace('RPS.', '') + ".")
        print("")

        def decide_winner(player, computerchoice):
            nonlocal player_wins
            nonlocal python_wins

            if (player == computerchoice):
                return "Stalemate! 🌚"
            if (player == 1 and computerchoice == 2):
                python_wins += 1
                return "Paper beats rock Python wins! 🐍"
            if (player == 1 and computerchoice == 3):
                player_wins += 1
                return "Rock beats scissors you win! 🎉"
            if (player == 2 and computerchoice == 1):
                player_wins += 1
                return "Paper beats rock you win! 🎉"
            if (player == 2 and computerchoice == 3):
                python_wins += 1
                return "Scissors beats paper Python Wins! 🐍"
            if (player == 3 and computerchoice == 1):
                python_wins += 1
                return "Rock beats scissors Python wins! 🐍"
            if (player == 3 and computerchoice == 2):
                player_wins += 1
                return "Scissors beats paper you win! 🎉"
            
        game_result = decide_winner(player, computerchoice)
        print(game_result)

        nonlocal game_count
        game_count += 1

        print("\nGame Count: " + str(game_count))
        print("\nPlayer Wins: " + str(player_wins))
        print("\nPython Wins: " + str(python_wins))

        print("\nPlay Again?")
        
        while True:
            playagain = input("\nY for Yes or \nQ to quit\n")
            if playagain.lower() not in ["y", "q"]:
                continue
            else:
                break

        if playagain.lower() == "y":
            return play_rps()
        else:
            print("\nThank you for Playing")
            sys.exit("Bye!")

    return play_rps


play = rps()

play()
