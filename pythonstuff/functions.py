def hello_world():
    print("hello world!")

hello_world()

def sum(num1, num2):
    print(num1 + num2)

sum(2, 2)

def sum_return(num1=0 , num2=0):
    if (type(num1) is not int or type(num2) is not int):
        return
    return num1 + num2 

total = sum_return(2,3)
print(total)

def multiple_items(*args):
    print(args)
    print(type(args))

multiple_items("johny" , "mark2")

def mult_names_items(**kwargs):
    print(kwargs)
    print(type(kwargs))

mult_names_items(first = "Dave", last ="Marsh")

