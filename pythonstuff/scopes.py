count = 1 

def test():
    color = "blue"
    global count
    count += 1
    print(count)

    def nested():
        nonlocal color
        print(color)
        color = "red"
        print(color)

    nested()

test()
