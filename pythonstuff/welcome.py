import math

meaning = 42
print('')

if meaning > 10:
    print("Right On!")
else: 
    print('No')

print(type(meaning))
print(type(meaning) == str)
print(isinstance(meaning, str))

# Constructor function 
pizza = str("Pepperoni")
print(type(pizza))
print(type(pizza) == str)
print(isinstance(pizza, str))

# Concatenation
first = "Mark"
last = "waster"
fullname = first + " " + last
print(fullname)

# Casting a number to a string 
decade = str(1980)
print(type(decade))
print(decade)

statement = "I like rock music from the " + decade + "s."
print(statement)

# Multiple Lines
multi_line = '''
Hey, how are you?

I was just checking in.

                        All good?
'''

print(multi_line)

# Escaping Special Characters
sentence = 'I\'m back at work! \tHey!\n\nWhere\'s this at\\located?'
print(sentence)

# String Methods
print(first)
print(first.lower())
print(first.upper())
print(first)

print(multi_line.title())
print(multi_line.replace("good", "ok"))

print(len(multi_line))
multi_line += "                      "
multi_line = "                   "  + multi_line
print(len(multi_line))

print(len(multi_line.strip()))
print(len(multi_line.lstrip()))
print(len(multi_line.rstrip()))

x = "x"
x += "y"
print(x)

print("")

# Build a menu 

title = "menu".upper()
print(title.center(20, "="))
print("Coffee".ljust(16, ".") + "$1".rjust(4))
print("Muffin".ljust(16, ".") + "$2".rjust(4))
print("Cheesecake".ljust(16, ".") + "$4".rjust(4))

#String Index values 

print("")

(print(first[1]))
print(first[-1]) # prints last value in index
print(first[1:-1]) # a range 
print(first[1:])

#Some methods return boolean data

print(first.startswith("M"))
print(first.startswith("X"))

# Boolean data value 
myvalue = True
x = bool(False)
print(type(x))
print(isinstance(myvalue, bool))

print("")
# float type
gpa = 3.28 
y = float(1.14)
print(type(gpa))

print("")

# complex type 
comp_value = 5+3j
print(type(comp_value))
print(comp_value.real)
print(comp_value.imag)

# Built in functions for numbers

print(abs(gpa))
print(round(gpa))
print(round(gpa, 1))

# math import
print(math.pi)


# casting a string to a number 
zipcode = "10001"
zip_value = int(zipcode)

print(type(zip_value))

# Error if you attempt to cast incorrect data 

zip_value = int("what")