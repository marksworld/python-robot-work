users = ['Dave', 'John', 'Sandra']

data = ['myname', 42 , True]

emptylist = [] 

print("Dave" in users)

print(users[0])

print(users[-1])

mylist = list([1, "neil" , True])

print(mylist)

# Tuples - lists cannot be changed
print("")

mytuple = tuple(('mark', 42 , False))

anothertuple = (1,4,2,5)

print(mytuple)
print(type(mytuple))
print(type(mytuple))

newlist = list(mytuple)
newlist.append('Neil')
newtuple = tuple(newlist)
print(newtuple)

(one, two, *what) = anothertuple
print(one)
print(two)
print(what)

