# user input rock paper scissors
import sys
import random
from enum import Enum

# recursion and while loop

game_count = 0

def play_rps():

    class RPS(Enum):
        ROCK = 1
        PAPER = 2
        SCISSORS = 3

    #print(RPS(2))
    #print(RPS.ROCK)
    #print(RPS['ROCK'])
    #print(RPS.ROCK.value)

    print("")

    playerchoice = input("Enter...\n1 for Rock,\n2 for paper,\n3 for scissors\n\n")

    if playerchoice not in ["1","2","3"]:
        print("You need to enter 1, 2 or 3.")
        return play_rps()

    player = int(playerchoice)

    computerchoice = int(random.choice("123"))

    print("")
    print("You chose " + str(RPS(player)).replace('RPS.', '') + ".")
    print("Python chose " + str(RPS(computerchoice)).replace('RPS.', '') + ".")
    print("")

    def decide_winner(player, computerchoice):

        if (player == computerchoice):
            return "Stalemate! 🌚"
        if (player == 1 and computerchoice == 2):
            return "Paper beats rock Python wins! 🐍"
        if (player == 1 and computerchoice == 3):
            return "Rock beats scissors you win! 🎉"
        if (player == 2 and computerchoice == 1):
            return "Paper beats rock you win! 🎉"
        if (player == 2 and computerchoice == 3):
            return "Scissors beats paper Python Wins! 🐍"
        if (player == 3 and computerchoice == 1):
            return "Rock beats scissors Python wins! 🐍"
        if (player == 3 and computerchoice == 2):
            return "Scissors beats paper you win! 🎉"
        
    game_result = decide_winner(player, computerchoice)
    print(game_result)

    global game_count
    game_count += 1

    print("\nGame Count: " + str(game_count))

    print("\nPlay Again?")
    
    while True:
        playagain = input("\nY for Yes or \nQ to quit\n")
        if playagain.lower() not in ["y", "q"]:
            continue
        else:
            break

    if playagain.lower() == "y":
        return play_rps()
    else:
        print("\nThank you for Playing")
        sys.exit("Bye!")


play_rps()

