from robot.api.deco import keyword

class Record:
    def __init__(self, key: str):
        self.key = key

@keyword("create record")
def create_record(key):
    return Record(key)

@keyword("do something")
def do_something(record: Record):
    x = "THIS IS RETURNED FROM THE PYTHON FUNCTION"
    return x
