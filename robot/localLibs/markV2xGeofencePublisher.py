from robot.api.deco import keyword, not_keyword
from pyaml_env import parse_config, BaseConfig
import uuid
import json
from datetime import datetime

systemVarFile = '../variables/system.yml'
headers = {"accept": "application/json"}

@not_keyword
def get_url():
    # this is used for set-up functions that are not actually keywords - used by other keywords to set-up stuff
    systemVars = BaseConfig(parse_config(systemVarFile))
    return getattr(systemVars, "v2n-geofence-tile-publisher")

def now():
    current_time = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3]
    return (current_time + "Z")

@keyword
def postData():
    get_url()

@keyword
def post_here_data():

    url = get_url() + "/here"
    headers = {
        'accept': 'application/json',
        'Content-Type': 'application/json'
        }
    
    pid = str(uuid.uuid4())
    timestamp = now()

    with open("../events/hereEvent.json") as file:
        hereData = json.load(file)

    hereData["pid"] = pid
    hereData["ts"] = timestamp
    hereData["payload"]["pid"] = pid
    hereData["payload"]["ts"] = timestamp

    payload = json.dumps(hereData)
    print(payload)

    #response = 


post_here_data()

x = get_url()
print(x)