import requests
from robot.api.deco import keyword
from requests.adapters import HTTPAdapter, Retry

s = requests.Session()

retries = Retry(
        total=3,
        status_forcelist=[ 500, 502, 503, 504, 400 ]
    )

s.mount('https://', HTTPAdapter(max_retries=retries))

@keyword("call endpoint")
def call_endpoint():
    URL = "https://room-service.eco-integration.pre-prod.jlr-vcdp.com/vehicle/E2EVIN21aa26ederw"

    PARAMS = {'address':"yep"}

    r = requests.get(url = URL, params = PARAMS)

    print(r)

    data = r.json()

    print(data['id'])

    return data

def retry_endpoint():

    URL = "https://petstore.swagger.io/v2/pet/9223372000001087713"

    response = s.get(URL)

    print(response)

retry_endpoint()


