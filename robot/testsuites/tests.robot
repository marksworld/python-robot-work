*** Settings ***
Library  SeleniumLibrary
Library  String
Library        ../localLibs/markV2xHive.py
Library        ../localLibs/callendpoints.py
Library        ../localLibs/object.py
Library    DateTime

Resource     ../resources/resources.robot
Resource     ../resources/system.robot

Suite Setup  Run Keywords  Log a message
Suite Teardown   

# Log a message is called in the suite setup from system.robot

*** Test Cases ***
Log out to console
    My Keyword
Log out this variable thing
    ${invoiceId}=    Generate Random String    10    [LETTERS]
    Log to Console  ${invoiceId}
My addition Test
    ${sum}=  Add  1  2
    Should be equal as integers  3  ${sum}

Addition Test from imported python function
    ${result} =    add function  1        2
    Should be equal as integers  3  ${result}

Read the Json response from GET Request
    ${response}=    call endpoint 

    Log To Console    ${response}

use a python object in robot
    # Set Test Variable    ${id}  123
    # ${test}=  Evaluate  Record(${id})

    ${sample1} =  create record    key

    Log To Console    ${sample1}
    Log To Console    hello
    
    ${what} =  do something    ${sample1}

    Log To Console    ${what}


# Robot passes them as a string and thats why it was failing! 

print date time
    ${datetime} = 	Get Current Date
    Log To Console    ${datetime}

*** Keywords ***

My Keyword
    Log to Console  This is weird ${name}
Add
  [Arguments]  ${value1}  ${value2}
  ${value}=  Evaluate  ${value1} + ${value2}
  [Return]  ${value}

*** Variables ***
${name}  Paul
${color}  teal
${team}   Jaguars

