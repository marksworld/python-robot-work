*** Keywords ***

Log a message
    Log to Console  "The test is starting with this message in the suite setup"

Generate Random Number
    ${random_number}    Evaluate    random.randint(1000000, 9999999)   random
    [return]    ${random_number}
